//
//  LatestState.swift
//  Insights
//
//  Created by Matthew Paletta on 2021-07-19.
//  Copyright © 2021 NumberEight Technologies Ltd. All rights reserved.
//

import Foundation

/// A helper class used to serialize the latest topic state of the subscribed topics within Insights.
class LatestEventState: Codable, Equatable {
    public var value: String
    public var confidence: Double

    init(value: String, confidence: Double) {
        self.value = value
        self.confidence = confidence
    }

    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.value = try container.decode(.value)
        self.confidence = try container.decode(.confidence)
    }

    enum CodingKeys: String, CodingKey {
        case value
        case confidence
    }

    static func == (lhs: LatestEventState, rhs: LatestEventState) -> Bool {
        return lhs.value == rhs.value && lhs.confidence == rhs.confidence
    }
}
