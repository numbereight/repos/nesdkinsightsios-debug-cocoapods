//
//  NEXIABAudience.h
//  Insights
//
//  Created by Matthew Paletta on 2021-09-06.
//  Copyright © 2021 NumberEight Technologies Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_SWIFT_NAME(IABAudience)
@interface NEXIABAudience : NSObject <NSSecureCoding>

@property(atomic, readonly, strong) NSString* _Nonnull ID;

@property(atomic, readonly, strong) NSArray<NSString*>* _Nonnull extensions;

/**
 * An extended identifier combining the audience ID with its extensions, as defined by
 * the IAB Seller-Defined Audiences guidance.
 *
 * Each extension is separated by a pipe character '|'.
 * The extensions are in alphabetical order.
 *
 * The extended ID is designed to be used instead of the ID when extensions are also desired.
 * This is useful for use cases such as adding audience memberships to ad requests.
 *
 * e.g.
 * - "408|PIFI1"
 * - "762|PIFI2|PIPV2"
 */
@property(nonatomic, readonly, nonnull) NSString* extendedId;

- (instancetype _Nonnull)init NS_UNAVAILABLE;

- (instancetype _Nonnull)initWithID:(NSString* _Nonnull)ID;
- (instancetype _Nonnull)initWithID:(NSString* _Nonnull)ID
                         extensions:(NSArray<NSString*>* _Nonnull)extensions;

/**
 * Returns an instance of NEXIABAudience from a dictionary.
 * @see `NEXIABAudience:asDictionary`
 */
+ (instancetype _Nullable)fromDictionary:(NSDictionary* _Nullable)dict;

- (NSString* _Nonnull)description;

- (NSDictionary* _Nonnull)asDictionary;

/**
 * @name NSSecureCoding
 */
- (instancetype _Nonnull)initWithCoder:(NSCoder* _Nullable)coder;
+ (BOOL)supportsSecureCoding;

- (BOOL)isEqual:(id _Nullable)object;

- (NSUInteger)hash;

@end
