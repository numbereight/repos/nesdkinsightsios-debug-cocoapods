//
//  NEXMembership.m
//  Insights
//
//  Created by Matthew Paletta on 2021-09-06.
//  Copyright © 2021 NumberEight Technologies Ltd. All rights reserved.
//

#import "NEXMembership.h"

#include <NumberEightCompiled/NELog.h>

// Encoding Keys
static NSString* const LOG_TAG = @"NEXMembership";
static NSString* const kIdKey = @"id";
static NSString* const kNameKey = @"name";
static NSString* const kIabIdsKey = @"iabIds";
static NSString* const kLivenessKey = @"liveness";

@interface NEXMembership ()

@property(readwrite, strong) NSString* _Nonnull ID;
@property(readwrite, strong) NSString* _Nonnull name;
@property(readwrite, strong) NSArray<NEXIABAudience*>* _Nonnull iabIds;
@property(readwrite) NEXLivenessState liveness;

@end

@implementation NEXMembership

@synthesize ID;
@synthesize name;
@synthesize iabIds;
@synthesize liveness;

- (instancetype)initWithID:(NSString* _Nonnull)initialID
                      name:(NSString* _Nonnull)initialName
                    iabIds:(NSArray<NEXIABAudience*>* _Nonnull)initialIabIds
                  liveness:(NEXLivenessState)initialLiveness {
    self = [super init];
    if (self) {
        self.ID = initialID;
        self.name = initialName;
        self.iabIds = initialIabIds;
        self.liveness = initialLiveness;
    }

    return self;
}

- (NSString* _Nonnull)extendedId {
    return
        [NSString stringWithFormat:@"%@|%c", self.ID, [NEXMembership livenessCode:self.liveness]];
}

- (NSString* _Nonnull)description {
    return self.name;
}

- (void)encodeWithCoder:(NSCoder*)encoder {
    [encoder encodeObject:self.ID forKey:kIdKey];
    [encoder encodeObject:self.name forKey:kNameKey];
    [encoder encodeObject:self.iabIds forKey:kIabIdsKey];
    [encoder encodeObject:[NEXMembership livenessToString:self.liveness] forKey:kLivenessKey];
}

- (NSUInteger)hash {
    NSUInteger hash = 17;
    hash = hash * 31 + [[NEXMembership class] hash];
    hash = hash * 31 + [self.ID hash];
    hash = hash * 31 + [self.name hash];
    hash = hash * 31 + [self.iabIds hash];
    hash = hash * 31 + [[NEXMembership livenessToString:self.liveness] hash];
    return hash;
}

- (BOOL)isEqual:(id)object {
    if (object == nil || ![object isKindOfClass:[NEXMembership class]]) {
        return false;
    }

    NEXMembership* other = (NEXMembership*)object;

    return [self.ID isEqual:other.ID] && [self.name isEqual:other.name] &&
           [self.iabIds isEqualToArray:other.iabIds] && self.liveness == other.liveness;
}

- (instancetype)initWithCoder:(NSCoder* _Nullable)coder {
    self = [super init];
    if (self) {
        self.ID = [coder decodeObjectOfClass:[NSString class] forKey:kIdKey];
        self.name = [coder decodeObjectOfClass:[NSString class] forKey:kNameKey];

        if (@available(iOS 14.0, *)) {
            NSArray* decodedIabIds = [coder decodeArrayOfObjectsOfClass:[NEXIABAudience class]
                                                                 forKey:kIabIdsKey];
            if (decodedIabIds != nil) {
                self.iabIds = (NSArray* _Nonnull)decodedIabIds;
            } else {
                self.iabIds = [NSArray new];
            }
        } else {
            self.iabIds = [coder
                decodeObjectOfClasses:[[NSSet alloc] initWithObjects:[NSArray class],
                                                                     [NEXIABAudience class], nil]
                               forKey:kIabIdsKey];
        }

        NSString* livenessStr = [coder decodeObjectOfClass:[NSString class] forKey:kLivenessKey];
        self.liveness = (NEXLivenessState)[
            [NEXMembership livenessStateFromString:livenessStr] unsignedIntValue];
    }
    return self;
}

+ (BOOL)supportsSecureCoding {
    return YES;
}

+ (NEXMembership* _Nullable)fromDictionary:(NSDictionary* _Nullable)dict {
    if (!dict) {
        return nil;
    }

    auto unableToParseKeyLog = [](NSString* key) -> NEXMembership* {
        [NELog msg:LOG_TAG
               error:@"Unable to parse key from JSON object."
            metadata:@{ @"key" : key }];
        return nil;
    };

    NSString* idParsed = dict[kIdKey];
    if (!idParsed) {
        return unableToParseKeyLog(kIdKey);
    }
    NSString* nameParsed = dict[kNameKey];
    if (!nameParsed) {
        return unableToParseKeyLog(kNameKey);
    }
    NSMutableArray* iabIdsParsed = [NSMutableArray new];
    {
        NSArray* iabIdsParsedDict = dict[kIabIdsKey];
        if (!iabIdsParsedDict) {
            return unableToParseKeyLog(kIabIdsKey);
        }
        for (NSDictionary* iabDict in iabIdsParsedDict) {
            if (auto parsed = [NEXIABAudience fromDictionary:iabDict]) {
                [iabIdsParsed addObject:parsed];
            } else {
                [NELog msg:LOG_TAG warning:@"Skipping IABAudience that was unable to be parsed."];
            }
        }
    }

    NEXLivenessState livenessParsed;
    {
        NSString* livenessParsedString = dict[kLivenessKey];
        if (!livenessParsedString) {
            return unableToParseKeyLog(kLivenessKey);
        }
        if (auto livenessVal = [NEXMembership livenessStateFromString:livenessParsedString]) {
            livenessParsed = (NEXLivenessState)[livenessVal unsignedIntValue];
        } else {
            [NELog msg:LOG_TAG
                   error:@"Invalid liveness string in fromDictionary."
                metadata:@{ @"liveness" : livenessParsedString }];
            return nil;
        }
    }

    return [[NEXMembership alloc] initWithID:idParsed
                                        name:nameParsed
                                      iabIds:iabIdsParsed
                                    liveness:livenessParsed];
}

- (NSDictionary*)asDictionary {
    NSMutableDictionary* output = [NSMutableDictionary new];
    [output setValue:self.ID forKey:kIdKey];
    [output setValue:self.name forKey:kNameKey];

    auto encodedIabIds = [&]() {
        // Loop over IAB ids and save their dictionary representation
        NSMutableArray* output = [NSMutableArray new];
        for (NEXIABAudience* iab in self.iabIds) {
            [output addObject:[iab asDictionary]];
        }
        return output;
    }();

    [output setValue:encodedIabIds forKey:kIabIdsKey];
    [output setValue:[NEXMembership livenessToString:self.liveness] forKey:kLivenessKey];

    return output;
}

+ (NSNumber* _Nullable)livenessStateFromString:(NSString*)liveness {
#define ParseLivenessCase(livenessState)                                             \
    if ([liveness isEqualToString:[NEXMembership livenessToString:livenessState]]) { \
        return [[NSNumber alloc] initWithUnsignedInteger:livenessState];             \
    }

    ParseLivenessCase(kNEXLivenessStateLive) ParseLivenessCase(kNEXLivenessStateHabitual)
        ParseLivenessCase(kNEXLivenessStateHappenedToday)
            ParseLivenessCase(kNEXLivenessStateHappenedThisWeek)
                ParseLivenessCase(kNEXLivenessStateHappenedThisMonth)

                    else {
        [NELog msg:LOG_TAG warning:@"Unknown Liveness type" metadata:@{ @"liveness" : liveness }];
        return nil;
    }

#undef ParseLivenessCase
}

+ (char)livenessCode:(NEXLivenessState)liveness {
    switch (liveness) {
        case kNEXLivenessStateLive:
            return 'L';
        case kNEXLivenessStateHappenedToday:
            return 'T';
        case kNEXLivenessStateHappenedThisWeek:
            return 'W';
        case kNEXLivenessStateHappenedThisMonth:
            return 'M';
        case kNEXLivenessStateHabitual:
            return 'H';
    }
}

+ (NSString* _Nonnull)livenessToString:(NEXLivenessState)liveness {
    switch (liveness) {
        case kNEXLivenessStateLive:
            return @"live";
        case kNEXLivenessStateHappenedToday:
            return @"today";
        case kNEXLivenessStateHappenedThisWeek:
            return @"this_week";
        case kNEXLivenessStateHappenedThisMonth:
            return @"this_month";
        case kNEXLivenessStateHabitual:
            return @"habitual";
    }
}

@end
