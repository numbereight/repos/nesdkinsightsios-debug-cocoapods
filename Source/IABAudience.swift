//
//  IABAudience.swift
//  Insights
//
//  Created by Matthew Paletta on 2021-03-12.
//  Copyright © 2021 NumberEight Technologies Ltd. All rights reserved.
//

import Foundation

extension IABAudience: Codable {
    enum CodingKeys: CodingKey {
        case id, extensions
    }

    public required convenience init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let id = try container.decode(String.self, forKey: .id)
        let extensions = try container.decode([String].self, forKey: .extensions)
        self.init(id: id, extensions: extensions)
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(self.id, forKey: .id)
        try container.encode(self.extensions as NSArray as? [String], forKey: .extensions)
    }

}
