//
//  Insights.swift
//  Insights
//
//  Created by Oliver Kocsis on 09/10/2019.
//  Copyright © 2019 NumberEight Technologies Ltd. All rights reserved.
//

import Foundation
import NumberEight

internal protocol AppLifeCycleObserver {
    static func willEnterForeground(_ notification: NSNotification)
    static func didEnterBackground(_ notification: NSNotification)
}

@objc 
class AppLifeCycleProxy: NSObject {
    private static let LOG_TAG = "AppLifeCycleProxy"
    
    static var proxyTo: AppLifeCycleObserver.Type?

    class func start(proxyTo: AppLifeCycleObserver.Type) {
        guard self.proxyTo != proxyTo else {
            NELog.msg(LOG_TAG, debug: "proxy already registered", metadata: ["proxy": String(describing: proxyTo)])
            return
        }
        self.proxyTo = proxyTo
        let nc = NotificationCenter.default
        #if !os(watchOS)
        nc.addObserver(self,
                       selector: #selector(AppLifeCycleProxy.willEnterForeground(_:)),
                       name: UIApplication.willEnterForegroundNotification,
                       object: nil)

        nc.addObserver(self,
                       selector: #selector(AppLifeCycleProxy.didEnterBackground(_:)),
                       name: UIApplication.didEnterBackgroundNotification,
                       object: nil)
        #endif
    }

    class func stop() {
        guard self.proxyTo != nil else {
            NELog.msg(LOG_TAG, debug: "proxy already stopped", metadata: ["proxy": String(describing: proxyTo)])
            return
        }

        // swiftlint:disable notification_center_detachment
        NotificationCenter.default.removeObserver(self)
        // swiftlint:enable notification_center_detachment
        self.proxyTo = nil
    }

    @objc 
    class func willEnterForeground(_ notification: NSNotification) {
        guard let proxyTo = self.proxyTo else { return }
        proxyTo.self.willEnterForeground(notification)
    }

    @objc 
    class func didEnterBackground(_ notification: NSNotification) {
        guard let proxyTo = self.proxyTo else { return }
        proxyTo.self.didEnterBackground(notification)
    }
}
